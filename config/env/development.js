// Set the 'development' environment configuration object
module.exports = {
	db: 'mongodb://localhost/baza-danych-oferty-technologicznej-development',
	sessionSecret: 'developmentSessionSecret',
	uploadTo: './uploads/'
};