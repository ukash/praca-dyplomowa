// Load the module dependencies
const config = require('./config');
const mongoose = require('mongoose');

// Add bluebird library
mongoose.Promise = require('bluebird');

// Define the Mongoose configuration method
module.exports = function () {
	// Use Mongoose to connect to MongoDB
	const db = mongoose.connect(config.db);

	// Load models 
	require('../server/models/user.server.model');
	require('../server/models/department.server.model');
	require('../server/models/contactPerson.server.model');
	require('../server/models/file.server.model');
	require('../server/models/offer.server.model');

	// Return the Mongoose connection instance
	return db;
};