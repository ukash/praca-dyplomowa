// Load the module dependencies
const config = require('./config');
const express = require('express');
const morgan = require('morgan');
const compress = require('compression');
const bodyParser = require('body-parser');
const methodOverride = require('method-override');
const session = require('express-session');
const MongoStore = require('connect-mongo')(session);
const passport = require('passport');
const path = require('path');

// Define the Express configuration method
module.exports = function (db) {
  // Create a new Express application instance
  const app = express();

  // Use the 'NDOE_ENV' variable to activate the 'morgan' logger or 'compress' middleware
  if (process.env.NODE_ENV === 'development') {
    app.use(morgan('dev'));
  } else if (process.env.NODE_ENV === 'production') {
    app.use(compress());
  }

  // Use the 'body-parser' and 'method-override' middleware functions
  app.use(bodyParser.urlencoded({
    extended: true
  }));
  app.use(bodyParser.json());
  app.use(methodOverride());

	// Configure the MongoDB session storage
	const mongoStore = new MongoStore({
		mongooseConnection: db.connection
	});

  // Configure the 'session' middleware
	app.use(session({
		saveUninitialized: true,
		resave: true,
		secret: config.sessionSecret,
		store: mongoStore
	}));

  // Configure the Passport middleware
  app.use(passport.initialize());
  app.use(passport.session());

  // Angular DIST output folder
  app.use(express.static(path.resolve('./dist')));

  // Load the API routing files
  require('../server/routes/users.server.routes')(app);
  require('../server/routes/departments.server.routes')(app);
  require('../server/routes/contactPeople.server.routes')(app);
  require('../server/routes/offers.server.routes')(app);
  require('../server/routes/files.server.routes')(app);
  //require('../server/routes/index.server.routes')(app);

  // Catch all other routes and return the index file
  app.get('*', (req, res) => {
    res.sendFile(path.resolve('./dist/index.html'));
  });

  // Return the Express application instance
  return app;
};
