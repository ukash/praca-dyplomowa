import { Component, OnInit } from '@angular/core';
import { User } from './user';
import { UserService } from './user.service';
import { FormControl, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { AppUsersRemoveDialogComponent } from './remove-dialog/remove-dialog.component';
import { MatSnackBar } from '@angular/material';

const EMAIL_REGEX = /^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css'],
  providers: [UserService]
})
export class UsersComponent implements OnInit {

  users: User[];
  selectedUser: User;
  ranks = [{
    'name': 'Użytkownik',
    'role': 'user'
  }, {
    'name': 'Administrator',
    'role': 'admin'
  }];
  hidePassword = true;
  saveLoading = false;

  emailFormControl = new FormControl('', [
    Validators.required,
    Validators.pattern(EMAIL_REGEX)
  ]);

  firstNameFormControl = new FormControl('', [
    Validators.required,
  ]);
  lastNameFormControl = new FormControl('', [
    Validators.required,
  ]);
  passwordFormControl = new FormControl('', [
    Validators.minLength(7)
  ]);

  constructor(
    private userService: UserService,
    public dialog: MatDialog,
    public snackbar: MatSnackBar
  ) {
    this.selectedUser = new User();
    this.selectedUser.role = 'user';
  }

  ngOnInit() {
    this.getUsers();
  }

  openRemoveDialog(user): void {
    const dialogRef = this.dialog.open(AppUsersRemoveDialogComponent, {
      data: user
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        user.removeLoading = true;
        this.delete(user);
      }
    });
  }

  getUsers() {
    this.userService.getUsers().then(users => this.users = users);
  }

  save(user: User) {
    this.saveLoading = true;
    if (user._id) {
      this.update(user);
    } else { this.add(user); }
  }

  clean() {
    this.selectedUser = new User();
    this.selectedUser.role = 'user';
    this.emailFormControl.markAsUntouched();
    this.passwordFormControl.markAsUntouched();
    this.firstNameFormControl.markAsUntouched();
    this.lastNameFormControl.markAsUntouched();
  }

  add(user: User): void {

    if (!user) { return; }
    this.userService.create(user)
      .then(res => {

        this.users.push(res);
        this.clean();
        this.saveLoading = false;

        this.snackbar.open('Nowy użytkownik został dodany.', 'OK', {
          duration: 3000
        });

      }, error => {
        this.saveLoading = false;
        this.snackbar.open('BŁĄD: ' + error, 'OK', {
          duration: 5000
        });
      });
  }

  update(user: User): void {
    this.userService.update(user)
      .then(res => {

        const index = this.users.findIndex(d => d._id === user._id);
        this.users[index] = Object.assign({}, res);
        this.clean();
        this.saveLoading = false;

        this.snackbar.open('Dane użytkownika zostały zmodyfikowane.', 'OK', {
          duration: 3000
        });

      }, error => {
        this.saveLoading = false;
        this.snackbar.open('BŁĄD: ' + error, 'OK', {
          duration: 5000
        });
      });
  }

  delete(user): void {

    const index = this.users.indexOf(user, 0);

    if (index > -1) {
      this.userService.delete(user._id)
        .then(res => {

          this.users.splice(index, 1);

          if (user === this.selectedUser) {
            this.selectedUser._id = null;
          }

          this.snackbar.open('Użytkownik został usunięty.', 'OK', {
            duration: 3000
          });

        }, error => {
          user.removeLoading = undefined;
          this.snackbar.open('BŁĄD: ' + error, 'OK', {
            duration: 5000
          });
        });
    }
  }

  onSelect(user: User): void {
    this.selectedUser = user;
  }

}
