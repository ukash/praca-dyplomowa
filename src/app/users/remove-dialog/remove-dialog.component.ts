import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-users-remove-dialog',
  templateUrl: './remove-dialog.component.html'
})
export class AppUsersRemoveDialogComponent {
  constructor( @Inject(MAT_DIALOG_DATA) public data: any) { }
}
