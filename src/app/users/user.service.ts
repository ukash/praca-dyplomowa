import 'rxjs/add/operator/toPromise';

import { Injectable } from '@angular/core';
import { User } from './user';
import { Http, Headers } from '@angular/http';

@Injectable()
export class UserService {

    private usersUrl = 'api/users';
    private headers = new Headers({ 'Content-Type': 'application/json' });

    constructor(private http: Http) { }

    private handleError(error: any): Promise<any> {
        return Promise.reject(error.json().message || 'Server error');
    }

    getUsers(): Promise<User[]> {
        return this.http.get(this.usersUrl)
            .toPromise()
            .then(res => res.json() as User[])
            .catch(this.handleError);
    }

    create(user: User): Promise<User> {
        return this.http.post(this.usersUrl, JSON.stringify({
            firstName: user.firstName,
            lastName: user.lastName,
            email: user.email,
            role: user.role,
            password: user.password
        }),
            { headers: this.headers })
            .toPromise()
            .then(res => res.json() as User)
            .catch(this.handleError);
    }

    update(user: User): Promise<User> {
        const url = this.usersUrl + '/' + user._id;
        return this.http.put(url, user, { headers: this.headers })
            .toPromise()
            .then(res => res.json() as User)
            .catch(this.handleError);
    }

    delete(id: string): Promise<User> {
        const url = this.usersUrl + '/' + id;
        return this.http.delete(url, { headers: this.headers })
            .toPromise()
            .then(res => res.json() as User)
            .catch(this.handleError);
    }

}
