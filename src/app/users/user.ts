export class User {
  _id: string;
  firstName: string;
  lastName: string;
  fullName: string;
  role: string;
  password: string;
  email: string;
}
