import { Injectable } from '@angular/core';
import {
    CanActivate, Router,
    ActivatedRouteSnapshot,
    RouterStateSnapshot
} from '@angular/router';
import { AuthenticationService } from './authentication/authentication.service';
import { Observable } from 'rxjs/Observable';


@Injectable()
export class AuthGuard implements CanActivate {
    constructor(public authenticationService: AuthenticationService, private router: Router) { }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | boolean {
        const url: string = state.url;

        return this.checkLogin(url);
    }

    checkLogin(url: string): Observable<boolean> {

        return this.authenticationService.isLoggedIn().map((isLoggedIn) => {
            if (isLoggedIn) { return true; }

            // Store the attempted URL for redirecting
            this.authenticationService.redirectUrl = url;

            // Navigate to the login page with extras
            this.router.navigate(['/authentication/signin']);
            return false;
        });
    }
}
