export class Department {
  _id: string;
  name: string;
  website: string;
  initialism: string;
}
