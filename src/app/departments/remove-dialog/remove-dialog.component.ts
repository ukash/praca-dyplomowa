import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-departments-remove-dialog',
  templateUrl: './remove-dialog.component.html'
})
export class AppDepartmentsRemoveDialogComponent {
  constructor( @Inject(MAT_DIALOG_DATA) public data: any) { }
}
