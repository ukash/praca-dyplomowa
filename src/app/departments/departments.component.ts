import { Component, OnInit } from '@angular/core';
import { Department } from './department';
import { DepartmentService } from './department.service';
import { FormControl, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { AppDepartmentsRemoveDialogComponent } from './remove-dialog/remove-dialog.component';
import { MatSnackBar } from '@angular/material';

const URL_REGEX = /[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/;

@Component({
  selector: 'app-departments',
  templateUrl: './departments.component.html',
  styleUrls: ['./departments.component.css'],
  providers: [DepartmentService]
})
export class DepartmentsComponent implements OnInit {
  departments: Department[];
  selectedDepartment: Department;
  saveLoading = false;

  urlFormControl = new FormControl('', [
    Validators.required,
    Validators.pattern(URL_REGEX)]);

  nameFormControl = new FormControl('', [
    Validators.required,
  ]);

  constructor(
    private departmentService: DepartmentService,
    public dialog: MatDialog,
    public snackbar: MatSnackBar
  ) {
    this.selectedDepartment = new Department();
  }

  ngOnInit() {
    this.getDepartments();
  }

  openRemoveDialog(department): void {
    const dialogRef = this.dialog.open(AppDepartmentsRemoveDialogComponent, {
      data: department
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        department.removeLoading = true;
        this.delete(department);
      }
    });
  }

  getDepartments() {
    this.departmentService.getDepartments().then(departments => this.departments = departments);
  }

  save(department: Department) {
    this.saveLoading = true;
    if (department._id) {
      this.update(department);
    } else { this.add(department); }
  }

  clean() {
    this.selectedDepartment = new Department();
    this.urlFormControl.markAsUntouched();
    this.nameFormControl.markAsUntouched();
  }

  add(department: Department): void {

    if (!department) { return; }
    this.departmentService.create(department)
      .then(res => {

        this.departments.push(res);
        this.clean();
        this.saveLoading = false;

        this.snackbar.open('Nowa katedra została dodana.', 'OK', {
          duration: 3000
        });

      }, error => {
        this.saveLoading = false;
        this.snackbar.open('BŁĄD: ' + error, 'OK', {
          duration: 5000
        });
      });
  }

  update(department: Department): void {
    this.departmentService.update(department)
      .then(res => {

        const index = this.departments.findIndex(d => d._id === department._id);
        this.departments[index] = Object.assign({}, res);
        this.clean();
        this.saveLoading = false;

        this.snackbar.open('Dane katedry zostały zmodyfikowane.', 'OK', {
          duration: 3000
        });

      }, error => {
        this.saveLoading = false;
        this.snackbar.open('BŁĄD: ' + error, 'OK', {
          duration: 5000
        });
      });
  }

  delete(department): void {

    const index = this.departments.indexOf(department, 0);

    if (index > -1) {
      this.departmentService.delete(department._id)
        .then(res => {

          this.departments.splice(index, 1);

          if (department === this.selectedDepartment) {
            this.selectedDepartment._id = null;
          }

          this.snackbar.open('Katedra została usunięta.', 'OK', {
            duration: 3000
          });

        }, error => {
          department.removeLoading = undefined;
          this.snackbar.open('BŁĄD: ' + error, 'OK', {
            duration: 5000
          });
        });
    }
  }

  onSelect(department: Department): void {
    this.selectedDepartment = department;
  }

}
