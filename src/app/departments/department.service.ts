import 'rxjs/add/operator/toPromise';

import { Injectable } from '@angular/core';
import { Department } from './department';
import { Http, Headers } from '@angular/http';

@Injectable()
export class DepartmentService {

  private departmentsUrl = 'api/departments';
  private headers = new Headers({ 'Content-Type': 'application/json' });

  constructor(
    private http: Http
  ) { }

  private handleError(error: any): Promise<any> {
    return Promise.reject(error.json().message || 'Server error');
  }

  getDepartments(): Promise<Department[]> {
    return this.http.get(this.departmentsUrl)
      .toPromise()
      .then(res => res.json() as Department[])
      .catch(this.handleError);
  }

  create(department: Department): Promise<Department> {
    return this.http.post(this.departmentsUrl, JSON.stringify({
      name: department.name,
      website: department.website
    }),
      { headers: this.headers })
      .toPromise()
      .then(res => res.json() as Department)
      .catch(this.handleError);
  }

  update(department: Department): Promise<Department> {
    const url = this.departmentsUrl + '/' + department._id;
    return this.http.put(url, department, { headers: this.headers })
      .toPromise()
      .then(res => res.json() as Department)
      .catch(this.handleError);
  }

  delete(id: string): Promise<Department> {
    const url = this.departmentsUrl + '/' + id;
    return this.http.delete(url, { headers: this.headers })
      .toPromise()
      .then(res => res.json() as Department)
      .catch(this.handleError);
  }

}
