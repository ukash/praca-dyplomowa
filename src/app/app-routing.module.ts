import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { OffersComponent } from './offers/offers.component';
import { AppComponent } from './app.component';
import { OfferDetailsComponent } from './offers/offer-details/offer-details.component';
import { SaveOfferComponent } from './offers/save-offer/save-offer.component';
import { DepartmentsComponent } from './departments/departments.component';
import { ContactPeopleComponent } from './contact-people/contact-people.component';
import { UsersComponent } from './users/users.component';
import { AuthGuard } from './auth-guard.service';
import { AdminGuard } from './admin-guard.service';

const routes: Routes = [
  { path: '', redirectTo: 'offers', pathMatch: 'full' },
  { path: 'offers', component: OffersComponent },
  { path: 'offers/details/:id', component: OfferDetailsComponent },
  { path: 'offers/add', component: SaveOfferComponent, canActivate: [AuthGuard] },
  { path: 'offers/edit/:id', component: SaveOfferComponent, canActivate: [AuthGuard] },
  { path: 'departments', component: DepartmentsComponent, canActivate: [AuthGuard, AdminGuard] },
  { path: 'contactPeople', component: ContactPeopleComponent, canActivate: [AuthGuard] },
  { path: 'users', component: UsersComponent, canActivate: [AuthGuard, AdminGuard] },
  { path: '**', component: OffersComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule { }
