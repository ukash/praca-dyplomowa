export class File {
    _id: string;
    originalname: string;
    mimetype: string;
    size: number;
}
