import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { File } from './file';

import 'rxjs/add/operator/toPromise';

@Injectable()
export class FileService {

    private fileUrl = 'api/files';
    private headers = new Headers({ 'Content-Type': 'application/json' });

    constructor(private http: Http) { }

    private handleError(error: any): Promise<any> {
        return Promise.reject(error.json().message || 'Server error');
    }

    getFile(id: string): Promise<File> {
        const url = `${this.fileUrl}/${id}`;

        return this.http.get(url)
            .toPromise()
            .then(response => response.json() as File)
            .catch(this.handleError);
    }

    uploadFile(fileData: FormData): Promise<File> {
        return this.http
            .post(this.fileUrl, fileData)
            .toPromise()
            .then(res => res.json() as File)
            .catch(this.handleError);
    }

    downloadFile(id: string) {
        const url = `${this.fileUrl}/download/${id}`;
        window.location.assign(url);
    }

    delete(id: string): Promise<void> {
        const url = `${this.fileUrl}/${id}`;
        return this.http
            .delete(url, { headers: this.headers })
            .toPromise()
            .then(() => null)
            .catch(this.handleError);
    }
}
