import { Component, OnInit } from '@angular/core';
import { User } from '../users/user';
import { AuthenticationService } from '../authentication/authentication.service';
import { Subject } from 'rxjs/Subject';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css']
})
export class NavComponent implements OnInit {

  public static updateUserStatus: Subject<null> = new Subject();
  user: User;
  title = 'Baza danych oferty technologicznej';

  constructor(private authenticationService: AuthenticationService) {
    NavComponent.updateUserStatus.subscribe(() => {
      this.user = this.authenticationService.user;
    });
  }

  ngOnInit() {
    this.authenticationService.checkStatus().subscribe(user => this.user = user);
  }

}
