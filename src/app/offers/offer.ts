import { Department } from '../departments/department';
import { ContactPerson } from '../contact-people/contactPerson';
import { User } from '../users/user';
import { TechDetail } from './tech-detail';
import { Patent } from './patent';
import { File } from '../files/file';

export class Offer {
  _id: string;
  extId: number;
  topic: string;
  abstract: string;
  benefits: string[];
  specification: string;
  techDetails: TechDetail[];
  applications: string[];
  trl: string;
  patents: Patent[];
  ipStatuses: string[];
  partnerships: string[];
  department: Department;
  attachments: File[];
  contactPeople: ContactPerson[];
  tags: string[];
  relatedOffers: Offer[];
  visible: boolean;
  createdBy: User;
  updatedBy: User;
  updatedAt: Date;
  createdAt: Date;

  constructor() {
    this.benefits = new Array<string>();
    this.techDetails = new Array<TechDetail>();
    this.applications = new Array<string>();
    this.patents = new Array<Patent>();
    this.ipStatuses = new Array<string>();
    this.partnerships = new Array<string>();
    this.attachments = new Array<File>();
    this.contactPeople = new Array<ContactPerson>();
    this.tags = new Array<string>();
    this.relatedOffers = new Array<Offer>();
    this.visible = true;
  }
}
