import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { Offer } from './offer';

import 'rxjs/add/operator/toPromise';
import { Tag } from './tag';

@Injectable()
export class OfferService {

  private offerUrl = 'api/offers';
  private headers = new Headers({ 'Content-Type': 'application/json' });

  constructor(private http: Http) { }

  private handleError(error: any): Promise<any> {
    return Promise.reject(error.json().message || 'Server error');
  }

  getOffers(less: boolean = false): Promise<Offer[]> {
    const url = (less ? `${this.offerUrl}/less` : this.offerUrl);

    return this.http.get(url)
      .toPromise()
      .then(response => response.json() as Offer[])
      .catch(this.handleError);
  }

  getOffer(id: string): Promise<Offer> {
    const url = `${this.offerUrl}/${id}`;

    return this.http.get(url)
      .toPromise()
      .then(response => response.json() as Offer)
      .catch(this.handleError);
  }

  getOffersTags(): Promise<Tag[]> {
    const url = `${this.offerUrl}/tags`;
    return this.http.get(url)
      .toPromise()
      .then(response => response.json() as Tag[])
      .catch(this.handleError);
  }

  delete(id: string): Promise<void> {
    const url = `${this.offerUrl}/${id}`;
    return this.http
      .delete(url, { headers: this.headers })
      .toPromise()
      .then(() => null)
      .catch(this.handleError);
  }

  create(offer: Offer): Promise<Offer> {
    return this.http
      .post(this.offerUrl, offer, { headers: this.headers })
      .toPromise()
      .then(res => res.json() as Offer)
      .catch(this.handleError);
  }

  update(offer: Offer): Promise<Offer> {
    const url = this.offerUrl + '/' + offer._id;
    return this.http.put(url, offer, { headers: this.headers })
      .toPromise()
      .then(res => res.json() as Offer)
      .catch(this.handleError);
  }
}
