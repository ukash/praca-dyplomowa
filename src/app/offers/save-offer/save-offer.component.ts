import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { OfferService } from '../offer.service';
import { Router, ParamMap, ActivatedRoute, Params } from '@angular/router';
import { Location } from '@angular/common';
import { Offer } from '../offer';
import { Department } from '../../departments/department';
import { DepartmentService } from '../../departments/department.service';
import { ContactPersonService } from '../../contact-people/contact-person.service';
import { ContactPerson } from '../../contact-people/contactPerson';
import { FormControl, Validators } from '@angular/forms';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/startWith';
import 'rxjs/add/operator/map';
import { TechDetail } from '../tech-detail';
import { MatSnackBar, MatChipInputEvent } from '@angular/material';
import { Patent } from '../patent';
import { Tag } from '../tag';
import { element } from 'protractor';
import { FileService } from '../../files/file.service';
import { File } from '../../files/file';

const COMMA = 188,
  ENTER = 13,
  URL_REGEX = /[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/;

@Component({
  selector: 'app-save-offer',
  templateUrl: './save-offer.component.html',
  styleUrls: ['./save-offer.component.css'],
  providers: [OfferService, DepartmentService, ContactPersonService, FileService]
})
export class SaveOfferComponent implements OnInit {
  offer: Offer;
  departments: Department[];
  contactPeople: ContactPerson[];
  tag: Tag;
  tags: Tag[];
  techDetail: TechDetail;
  patent: Patent;
  benefit: string;
  application: string;
  trl: string;
  trls: string[];
  relatedOffers: Offer[];
  customIpStatus: DropboxItem;
  customPartnership: DropboxItem;
  ipStatuses: DropboxItem[];
  partnerships: DropboxItem[];
  contactPeopleFormControl: FormControl;
  tagsFormControl: FormControl;
  topicFormControl: FormControl;
  trlFormControl: FormControl;
  abstractFormControl: FormControl;
  relatedOffersFormControl: FormControl;
  patentUrlFormControl: FormControl;
  departmentFormControl: FormControl;
  filteredContactPeople: Observable<ContactPerson[]>;
  filteredTags: Observable<Tag[]>;
  filteredRelatedOffers: Observable<Offer[]>;
  uploadLoading: number;
  saveLoading: boolean;

  // Enter, comma
  separatorKeysCodes = [ENTER, COMMA];

  @ViewChild('fileInput') fileInput;

  constructor(
    private offerService: OfferService,
    private departmentService: DepartmentService,
    private contactPersonService: ContactPersonService,
    private fileService: FileService,
    private location: Location,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    public snackbar: MatSnackBar
  ) {

    this.offer = new Offer();

    this.contactPeopleFormControl = new FormControl();
    this.relatedOffersFormControl = new FormControl();
    this.tagsFormControl = new FormControl();
    this.departmentFormControl = new FormControl('', [
      Validators.required,
    ]);
    this.topicFormControl = new FormControl('', [
      Validators.required,
    ]);
    this.trlFormControl = new FormControl('', [
      Validators.required,
    ]);
    this.abstractFormControl = new FormControl('', [
      Validators.required
    ]);
    this.patentUrlFormControl = new FormControl('', [
      Validators.pattern(URL_REGEX)
    ]);
    this.techDetail = new TechDetail();
    this.patent = new Patent();

    this.trls = [
      '1 - Zaobserwowano podstawowe zasady danego zjawiska',
      '2 - Sformułowano koncepcję technologiczną',
      '3 - Przeprowadzono eksperymentalny dowód na słuszność koncepcji',
      '4 - Przeprowadzono walidację technologii w warunkach laboratoryjnych',
      '5 - Dokonano walidacji technologii w środowisku zbliżonym do rzeczywistego',
      '6 - Dokonano demonstracji technologii w środowisku zbliżonym do rzeczywistego',
      '7 - Dokonano demonstracji prototypu systemu w otoczeniu operacyjnym',
      '8 - Zakończono badania i demonstrację ostatecznej formy technologii',
      '9 - Działanie systemu udowodniono w środowisku operacyjnym i uruchomiono produkcję na skalę przemysłową'
    ];

    this.ipStatuses = new Array<DropboxItem>();
    this.ipStatuses.push(
      { name: 'Zgłoszenie patentowe', selected: false },
      { name: 'Patent', selected: false },
      { name: 'Know-how', selected: false }
    );
    this.customIpStatus = new DropboxItem('', true);

    this.partnerships = new Array<DropboxItem>();
    this.partnerships.push(
      { name: 'Współpraca techniczna', selected: false },
      { name: 'Umowa Joint Venture', selected: false },
      { name: 'Umowa produkcyjna (wykonawstwo i podwykonawstwo)', selected: false },
      { name: 'Umowa handlowa przy asyście technicznej', selected: false },
      { name: 'Umowa licencyjna', selected: false }
    );
    this.customPartnership = new DropboxItem('', true);

    this.contactPeople = [];
    this.tags = [];
    this.relatedOffers = [];

    this.uploadLoading = 0;
    this.saveLoading = false;

  }

  ngOnInit() {
    this.getDepartments();
    this.getContactPeople();
    this.getOffersTags();
    this.getRelatedOffers();

    this.activatedRoute.params.subscribe((params: Params) => params['id'] ? this.getOffer(params['id']) : null);

    this.filteredContactPeople = this.contactPeopleFormControl.valueChanges
      .startWith(null)
      .map(contactPerson => contactPerson && typeof contactPerson === 'object' ? contactPerson.fullName : contactPerson)
      .map(name => this.contactPeopleFilter(name));

    this.filteredTags = this.tagsFormControl.valueChanges
      .startWith(null)
      .map(tag => tag && typeof tag === 'object' ? tag._id : tag)
      .map(name => this.tagsFilter(name));

    this.filteredRelatedOffers = this.relatedOffersFormControl.valueChanges
      .startWith(null)
      .map(relatedOffer => relatedOffer &&
        typeof relatedOffer === 'object' ? `${relatedOffer.extId} ${relatedOffer.topic}` : relatedOffer)
      .map(name => this.relatedOffersFilter(name));
  }

  download(file: File) {
    this.fileService.downloadFile(file._id);
  }

  upload() {
    if (this.fileInput.nativeElement.files.length) {
      for (const file of this.fileInput.nativeElement.files) {
        this.uploadLoading++;
        const formData = new FormData();
        formData.append('attachment', file);
        this.uploadFile(formData);
      }
    }

    this.fileInput.nativeElement.value = '';
  }

  removeFile(file: File) {
    this.fileService.delete(file._id).then(() => {

      const index = this.offer.attachments.indexOf(file);

      if (index > -1) {
        this.offer.attachments.splice(index, 1);
      }

      this.snackbar.open(`Plik ${file.originalname} został usunięty.`, 'OK', {
        duration: 3000
      });
    }, error => {
      this.snackbar.open('BŁĄD: ' + error, 'OK', {
        duration: 5000
      });
    });
  }


  relatedOffersFilter(name: string): Offer[] {
    return this.relatedOffers
      .filter(relatedOffer => !this.offer.relatedOffers.some(elem => elem._id === relatedOffer._id))
      .filter(relatedOffer => {
        const str = `${relatedOffer.extId} ${relatedOffer.topic}`;
        return str.toLowerCase().indexOf((name || '').toLowerCase()) !== -1;
      });
  }

  contactPeopleFilter(name: string): ContactPerson[] {
    return this.contactPeople
      .filter(contactPerson => !this.offer.contactPeople.some(elem => elem._id === contactPerson._id))
      .filter(contactPerson => contactPerson.fullName.toLowerCase().indexOf((name || '').toLowerCase()) !== -1);
  }

  tagsFilter(name: string): Tag[] {
    return this.tags
      .filter(tag =>
        !this.offer.tags.includes(tag._id))
      .filter(tag =>
        tag._id.toLowerCase().indexOf((name || '').toLowerCase()) !== -1);
  }

  addPartnership(partnership: DropboxItem) {
    const index = this.offer.partnerships.indexOf(partnership.name);

    if (index === -1) {
      this.offer.partnerships.push(partnership.name);
    } else {
      this.offer.partnerships.splice(index, 1);
    }
  }

  addIpStatus(ipStatus: DropboxItem) {
    const index = this.offer.ipStatuses.indexOf(ipStatus.name);

    if (index === -1) {
      this.offer.ipStatuses.push(ipStatus.name);
    } else {
      this.offer.ipStatuses.splice(index, 1);
    }
  }

  compareFn(c1: Department, c2: Department): boolean {
    return c1 && c2 ? c1._id === c2._id : c1 === c2;
  }

  addRelatedOfferChip(relatedOffer: Offer, input: HTMLInputElement, event: any): void {
    if (relatedOffer && event.source.selected && !this.offer.relatedOffers.includes(relatedOffer)) {
      this.offer.relatedOffers.push(relatedOffer);
      input.value = null;
    }
  }

  removeRelatedOfferChip(relatedOffer: Offer): void {
    const index = this.offer.relatedOffers.indexOf(relatedOffer);

    if (index > -1) {
      this.offer.relatedOffers.splice(index, 1);
    }
  }

  addContactPersonChip(contactPerson: ContactPerson, input: HTMLInputElement, event: any): void {
    if (contactPerson && event.source.selected && !this.offer.contactPeople.includes(contactPerson)) {
      this.offer.contactPeople.push(contactPerson);
      input.value = null;
    }
  }

  removeContactPersonChip(contactPerson: ContactPerson): void {
    const index = this.offer.contactPeople.indexOf(contactPerson);

    if (index > -1) {
      this.offer.contactPeople.splice(index, 1);
    }
  }

  addTag(event: MatChipInputEvent): void {
    const input = event.input;
    const value = event.value;

    // Add tag
    if ((value || '').trim() && !this.offer.tags.includes(value)) {
      this.offer.tags.push(value.trim());
    }

    // Reset the input value
    if (input) {
      input.value = '';
    }
  }

  addTagChip(tag: Tag, input: HTMLInputElement, event: any): void {
    if (tag && event.source.selected && !this.offer.tags.includes(tag._id)) {
      this.offer.tags.push(tag._id);
      input.value = '';
    }
  }

  removeTag(name: string): void {
    const index = this.offer.tags.indexOf(name);

    if (index > -1) {
      this.offer.tags.splice(index, 1);
    }
  }

  addCustomPartnership(): void {
    if (this.customPartnership.name && this.partnerships.indexOf(this.customPartnership) === -1) {
      this.partnerships.push(this.customPartnership);
      this.offer.partnerships.push(this.customPartnership.name);
    }

    this.customPartnership = new DropboxItem('', true);
  }

  addCustomIpStatus(): void {
    if (this.customIpStatus.name && this.ipStatuses.indexOf(this.customIpStatus) === -1) {
      this.ipStatuses.push(this.customIpStatus);
      this.offer.ipStatuses.push(this.customIpStatus.name);
    }

    this.customIpStatus = new DropboxItem('', true);
  }

  addTechDetail(): void {
    if (this.techDetail && this.offer.techDetails.indexOf(this.techDetail) === -1) {
      this.offer.techDetails.push(this.techDetail);
    }
    this.techDetail = new TechDetail();
  }

  removeTechDetail(techDetail: TechDetail): void {
    const index = this.offer.techDetails.indexOf(techDetail);

    if (index > -1) {
      this.offer.techDetails.splice(index, 1);
    }
  }

  onSelectTechDetail(techDetail: TechDetail): void {
    this.techDetail = techDetail;
  }

  addPatent(): void {
    if (this.patent && this.offer.patents.indexOf(this.patent) === -1) {
      this.offer.patents.push(this.patent);
    }
    this.patent = new Patent();
  }

  removePatent(patent: Patent): void {
    const index = this.offer.patents.indexOf(patent);

    if (index > -1) {
      this.offer.patents.splice(index, 1);
    }
  }

  onSelectPatent(patent: Patent): void {
    this.patent = patent;
  }

  addBenefit(): void {
    if (this.benefit && this.offer.benefits.indexOf(this.benefit) === -1) {
      this.offer.benefits.push(this.benefit);
    }
    this.benefit = '';
  }

  removeBenefit(benefit: string): void {
    const index = this.offer.benefits.indexOf(benefit);

    if (index > -1) {
      this.offer.benefits.splice(index, 1);
    }
  }

  onSelectBenefit(benefit: string): void {
    this.benefit = benefit;
  }

  addApplication(): void {
    if (this.application && this.offer.applications.indexOf(this.application) === -1) {
      this.offer.applications.push(this.application);
    }
    this.application = '';
  }

  removeApplication(application: string): void {
    const index = this.offer.applications.indexOf(application);

    if (index > -1) {
      this.offer.applications.splice(index, 1);
    }
  }

  onSelectApplication(application: string): void {
    this.application = application;
  }

  saveOffer(): void {
    this.saveLoading = true;
    if (this.offer._id) {
      this.updateOffer();
    } else { this.addOffer(); }
  }

  addOffer(): void {
    this.offerService.create(this.offer)
      .then(res => {
        this.snackbar.open(`Nowa oferta nr ${res.extId} została dodana.`, 'OK', {
          duration: 3000
        });

        this.router.navigate(['offers/details', res._id]);
      }, error => {
        this.saveLoading = false;
        this.snackbar.open('BŁĄD: ' + error, 'OK', {
          duration: 5000
        });
      });
  }

  updateOffer(): void {
    this.offerService.update(this.offer)
      .then(res => {
        this.snackbar.open(`Oferta nr ${res.extId} została zaktualizowana.`, 'OK', {
          duration: 3000
        });

        this.router.navigate(['offers/details', res._id]);
      }, error => {
        this.saveLoading = false;
        this.snackbar.open('BŁĄD: ' + error, 'OK', {
          duration: 5000
        });
      });
  }

  getDepartments() {
    this.departmentService.getDepartments().then(departments => this.departments = departments);
  }

  getOffer(id: string) {
    this.offerService.getOffer(id).then(offer => {
      this.offer = offer;
      this.offer.partnerships.forEach(offerPartnerShip => {
        for (const partnership of this.partnerships) {
          if (partnership.name === offerPartnerShip) {
            partnership.selected = true;
            return;
          }
        }
        this.partnerships.push(new DropboxItem(offerPartnerShip, true));
      });

      this.offer.ipStatuses.forEach(offerIpStatus => {
        for (const ipStatus of this.ipStatuses) {
          if (ipStatus.name === offerIpStatus) {
            ipStatus.selected = true;
            return;
          }
        }
        this.ipStatuses.push(new DropboxItem(offerIpStatus, true));
      });
    });
  }

  uploadFile(file: FormData) {
    this.fileService.uploadFile(file)
      .then(resFile => {
        this.uploadLoading--;
        this.offer.attachments.push(resFile);
        this.snackbar.open(`Plik ${resFile.originalname} został dodany.`, 'OK', {
          duration: 3000
        });
      }, error => {
        this.uploadLoading--;
        this.snackbar.open('BŁĄD: ' + error, 'OK', {
          duration: 5000
        });
      });
  }


  getContactPeople() {
    this.contactPersonService.getContactPeople().then(contactPeople => this.contactPeople = contactPeople);
  }

  getOffersTags() {
    this.offerService.getOffersTags().then(tags => this.tags = tags);
  }

  getRelatedOffers() {
    this.offerService.getOffers(true).then(offers => this.relatedOffers = offers);
  }
  goBack() {
    this.location.back();
  }
}

class DropboxItem {
  name: string;
  selected: boolean;

  constructor(name: string = '', selected: boolean = false) {
    this.name = name;
    this.selected = selected;
  }
}
