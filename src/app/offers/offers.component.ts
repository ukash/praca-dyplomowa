import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Offer } from './offer';
import { OfferService } from './offer.service';
import { Department } from '../departments/department';
import { ContactPerson } from '../contact-people/contactPerson';
import { Router } from '@angular/router';
import { AuthenticationService } from '../authentication/authentication.service';
import { DataSource } from '@angular/cdk/collections';
import { Observable } from 'rxjs/Observable';
import { MatPaginator, MatSort } from '@angular/material';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/startWith';
import 'rxjs/add/observable/merge';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/observable/fromEvent';
import { DepartmentService } from '../departments/department.service';
import { FormControl } from '@angular/forms';
import { ContactPersonService } from '../contact-people/contact-person.service';
import { User } from '../users/user';

@Component({
  selector: 'app-offers',
  templateUrl: './offers.component.html',
  styleUrls: ['./offers.component.css'],
  providers: [OfferService, DepartmentService, ContactPersonService]
})

export class OffersComponent implements OnInit {

  offers: Offer[];
  offersChange: BehaviorSubject<Offer[]> = new BehaviorSubject<Offer[]>(this.offers);
  selectedOffer: Offer;
  departments: Department[];
  contactPeople: ContactPerson[];
  dataSource: OffersDataSource;
  isLoggedIn: boolean;
  departmentsFormControl: FormControl;
  contactPeopleFormControl: FormControl;
  showInvisibleFormControl: FormControl;
  displayedColumns = ['extId', 'department', 'topic', 'trl', 'contactPeople'];
  showAdvancedOptions = false;
  showAdvancedIcon = 'keyboard_arrow_down';

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild('filter') filter: ElementRef;
  @ViewChild(MatSort) sort: MatSort;

  constructor(
    private offerService: OfferService,
    private router: Router,
    private departmentService: DepartmentService,
    private contactPersonService: ContactPersonService,
    private authenticationService: AuthenticationService
  ) {
    this.offers = new Array<Offer>();
    this.departments = new Array<Department>();
    this.contactPeople = new Array<ContactPerson>();
    this.departmentsFormControl = new FormControl();
    this.contactPeopleFormControl = new FormControl();
    this.showInvisibleFormControl = new FormControl();
    this.isLoggedIn = false;
  }

  ngOnInit() {
    this.getOffers();
    this.showInvisibleFormControl.setValue(true);
    this.authenticationService.isLoggedIn().subscribe(isLoggedIn => this.isLoggedIn = isLoggedIn);
  }

  getOffers() {
    this.offerService.getOffers().then(offers => {
      this.offers = offers;
      this.dataSource = new OffersDataSource(
        this.offers,
        this.offersChange,
        this.paginator,
        this.sort,
        this.contactPeopleFormControl,
        this.departmentsFormControl,
        this.showInvisibleFormControl
      );

      Observable.fromEvent(this.filter.nativeElement, 'keyup')
        .debounceTime(150)
        .distinctUntilChanged()
        .subscribe(() => {
          if (!this.dataSource) { return; }
          this.dataSource.filter = this.filter.nativeElement.value;
        });
    });
  }

  getDepartments() {
    this.departmentService.getDepartments().then(departments => this.departments = departments);
  }

  getContactPeople() {
    this.contactPersonService.getContactPeople().then(contactPeople => this.contactPeople = contactPeople);
  }

  showAdvanced(): void {
    if (this.showAdvancedOptions) {
      this.showAdvancedOptions = false;
      this.showAdvancedIcon = 'keyboard_arrow_down';
    } else {
      this.showAdvancedOptions = true;
      this.showAdvancedIcon = 'keyboard_arrow_up';
      if (!this.departments.length) { this.getDepartments(); }
      if (!this.contactPeople.length) { this.getContactPeople(); }
    }
  }

  gotoDetails(offer: Offer) {
    this.router.navigate(['/offers/details', offer._id]);
  }

}

export class OffersDataSource extends DataSource<any> {

  _selectedContactPeople = new Array<ContactPerson>();
  _selectedDepartments = new Array<Department>();

  _filterChange = new BehaviorSubject('');
  get filter(): string { return this._filterChange.value; }
  set filter(filter: string) { this._filterChange.next(filter); }

  constructor(
    private _offers: Offer[],
    private _offersChange: any,
    private _paginator: MatPaginator,
    private _sort: MatSort,
    private _contactPeopleFormControl: FormControl,
    private _departmentsFormControl: FormControl,
    private _showInvisibleFormControl: FormControl
  ) {
    super();
  }

  /** Connect function called by the table to retrieve one stream containing the data to render. */
  connect(): Observable<Offer[]> {
    const displayDataChanges = [
      this._offersChange,
      this._paginator.page,
      this._filterChange,
      this._sort.sortChange,
      this._contactPeopleFormControl.valueChanges,
      this._departmentsFormControl.valueChanges,
      this._showInvisibleFormControl.valueChanges
    ];

    return Observable.merge(...displayDataChanges).map(() => {
      const data = this._offers.slice()
        .filter((offer) => this._showInvisibleFormControl.value ? true : offer.visible) // filter invisible offers
        .filter((offer) => this._selectedDepartments.length ? // filter departments
          this._selectedDepartments.some(elem => elem._id === offer.department._id)
          : true)
        .filter((offer) => this._selectedContactPeople.length ? // filter contactPeople
          this._selectedContactPeople.some(elem => offer.contactPeople.some(contactPerson => contactPerson._id === elem._id))
          : true)
        .filter((offer) => {
          const searchStr = (offer.extId + offer.topic + offer.tags.toString()).toLowerCase();

          return searchStr.indexOf(this.filter.toLowerCase()) !== -1;
        });
      this._paginator.length = data.length; // set the new paginator length
      // Sort + Grab the page's slice of data.
      const startIndex = this._paginator.pageIndex * this._paginator.pageSize;
      return this.getSortedData(data).splice(startIndex, this._paginator.pageSize);
    });
  }

  disconnect() { }

  getSortedData(_data: Offer[]): Offer[] {
    const data = _data.slice();
    if (!this._sort.active || this._sort.direction === '') { return data; }

    return data.sort((a, b) => {
      let propertyA: number | string = '';
      let propertyB: number | string = '';

      switch (this._sort.active) {
        case 'extId': [propertyA, propertyB] = [a.extId, b.extId]; break;
        case 'trl': [propertyA, propertyB] = [a.trl, b.trl]; break;
        case 'topic': [propertyA, propertyB] = [a.topic, b.topic]; break;
      }

      const valueA = isNaN(+propertyA) ? propertyA : +propertyA;
      const valueB = isNaN(+propertyB) ? propertyB : +propertyB;

      return (valueA < valueB ? -1 : 1) * (this._sort.direction === 'desc' ? 1 : -1);
    });
  }
}
