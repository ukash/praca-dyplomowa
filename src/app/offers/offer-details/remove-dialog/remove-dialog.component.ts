import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material';
import { Offer } from '../../offer';

@Component({
  selector: 'app-offers-offer-details-remove-dialog',
  templateUrl: './remove-dialog.component.html',
  styleUrls: ['./remove-dialog.component.css']
})
export class AppOffersOfferDetailRemoveDialogComponent {

  constructor( @Inject(MAT_DIALOG_DATA) public offer: Offer) { }
}
