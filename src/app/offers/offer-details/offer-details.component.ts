import 'rxjs/add/operator/switchMap';

import { Component, OnInit } from '@angular/core';
import { Offer } from '../offer';
import { OfferService } from '../offer.service';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { Location } from '@angular/common';
import { AuthenticationService } from '../../authentication/authentication.service';
import { AppOffersOfferDetailRemoveDialogComponent } from './remove-dialog/remove-dialog.component';
import { MatDialog, MatSnackBar } from '@angular/material';
import { FileService } from '../../files/file.service';
import { File } from '../../files/file';

@Component({
  selector: 'app-offer-details',
  templateUrl: './offer-details.component.html',
  styleUrls: ['./offer-details.component.css'],
  providers: [OfferService, FileService]
})
export class OfferDetailsComponent implements OnInit {

  offer: Offer;
  isLoggedIn: boolean;
  removeLoading: boolean;

  constructor(
    private offerService: OfferService,
    private fileService: FileService,
    private route: ActivatedRoute,
    private location: Location,
    private authenticationService: AuthenticationService,
    private router: Router,
    public dialog: MatDialog,
    public snackbar: MatSnackBar
  ) {
    this.offer = new Offer();
    this.isLoggedIn = false;
    this.removeLoading = false;
  }

  ngOnInit() {
    this.route.paramMap
      .switchMap((params: ParamMap) => this.offerService.getOffer(params.get('id')))
      .subscribe(offer => this.offer = offer);
      this.authenticationService.isLoggedIn().subscribe(isLoggedIn => this.isLoggedIn = isLoggedIn);

  }

  delete(): void {

    this.offerService
      .delete(this.offer._id)
      .then(() => {
        this.snackbar.open(`Oferta nr ${this.offer.extId} została usunięta.`, 'OK', {
          duration: 3000
        });
        this.router.navigate(['/']);
      }, error => {
        this.removeLoading = false;
        this.snackbar.open('BŁĄD: ' + error, 'OK', {
          duration: 5000
        });
      });
  }

  openRemoveDialog(): void {
    const dialogRef = this.dialog.open(AppOffersOfferDetailRemoveDialogComponent, {
      data: this.offer
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.removeLoading = true;
        this.delete();
      }
    });
  }

  download(file: File) {
    this.fileService.downloadFile(file._id);
  }

  gotoOfferEdit() {
    this.router.navigate(['/offers/edit', this.offer._id]);
  }

  gotoRelatedOffer(id: string) {
    this.router.navigate(['/offers/details', id]);
  }

  goBack() {
    this.location.back();
  }

}
