import { NgModule } from '@angular/core';
import {
  MatButtonModule, MatCheckboxModule, MatToolbarModule, MatIconModule, MatIconRegistry,
  MatTooltipModule, MatTabsModule, MatListModule, MatCardModule, MatInputModule, MatSortModule,
  MatFormFieldModule, MatProgressSpinnerModule, MatDialogModule, MatSnackBarModule,
  MatSelectModule, MatChipsModule, MatAutocompleteModule, MatTableModule, MatPaginatorModule, MatPaginatorIntl

} from '@angular/material';

import { AppDepartmentsRemoveDialogComponent } from './departments/remove-dialog/remove-dialog.component';
import { AppContactPeopleRemoveDialogComponent } from './contact-people/remove-dialog/remove-dialog.component';
import { AppOffersOfferDetailRemoveDialogComponent } from './offers/offer-details/remove-dialog/remove-dialog.component';
import { AppUsersRemoveDialogComponent } from './users/remove-dialog/remove-dialog.component';

const materialModules = [
  MatButtonModule,
  MatCheckboxModule,
  MatToolbarModule,
  MatIconModule,
  MatTooltipModule,
  MatTabsModule,
  MatListModule,
  MatCardModule,
  MatInputModule,
  MatFormFieldModule,
  MatProgressSpinnerModule,
  MatDialogModule,
  MatSnackBarModule,
  MatSelectModule,
  MatChipsModule,
  MatAutocompleteModule,
  MatTableModule,
  MatPaginatorModule,
  MatSortModule
];

@NgModule({
  imports:
    [
      materialModules
    ],
  exports: [
    materialModules
  ],
  declarations: [
    AppDepartmentsRemoveDialogComponent,
    AppContactPeopleRemoveDialogComponent,
    AppOffersOfferDetailRemoveDialogComponent,
    AppUsersRemoveDialogComponent
  ],
  entryComponents: [
    AppDepartmentsRemoveDialogComponent,
    AppContactPeopleRemoveDialogComponent,
    AppOffersOfferDetailRemoveDialogComponent,
    AppUsersRemoveDialogComponent
  ],
  providers: [MatIconRegistry],

})
export class MaterialModule {
  constructor(
    public matIconRegistry: MatIconRegistry, matPaginatorIntl: MatPaginatorIntl) {
    matIconRegistry.registerFontClassAlias('fontawesome', 'fa');
    matPaginatorIntl.itemsPerPageLabel = 'Pozycji na stronie:';
    matPaginatorIntl.nextPageLabel = 'Następna strona';
    matPaginatorIntl.previousPageLabel = 'Poprzednia strona';
    matPaginatorIntl.getRangeLabel = function (page: number, pageSize: number, length: number) {
      if (length === 0 || pageSize === 0) { return `0 z ${length}`; }
      length = Math.max(length, 0); const startIndex = page * pageSize;
      // If the start index exceeds the list length, do not try and fix the end index to the end.
      const endIndex = startIndex < length ? Math.min(startIndex + pageSize, length) : startIndex + pageSize;
      return `${startIndex + 1} - ${endIndex} z ${length}`;
    };
  }
}
