import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { AuthenticationService } from './authentication/authentication.service';
import { MatSnackBar } from '@angular/material';
import { Observable } from 'rxjs/Observable';


@Injectable()
export class AdminGuard implements CanActivate {
    constructor(
        private authenticationService: AuthenticationService,
        private snackBar: MatSnackBar,
        private router: Router
    ) { }

    canActivate(): Observable<boolean> | boolean {
        return this.authenticationService.isAdmin().map((isAdmin) => {
            if (isAdmin) { return true; }

            this.snackBar.open('Brak dostępu. Wymagana ranga administrator.', 'OK', {
                duration: 5000
            });

            // Navigate to the main page
            this.router.navigate(['/']);

            return false;
        });
    }
}
