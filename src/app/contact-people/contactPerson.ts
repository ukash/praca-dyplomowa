export class ContactPerson {
  _id: string;
  firstName: string;
  lastName: string;
  fullName: string;
  title: titles;
  website: string;
}

const enum titles {
  'inż.', 'dypl. ek.', 'mgr', 'mgr inż.', 'mgr inż. architekt', 'dr',
  'dr inż.', 'dr hab.', 'dr hab. inż.', 'dr inż. architekt', 'prof. dr hab.',
  'prof. dr hab. inż.', 'prof. nadzw. dr hab.', 'prof. nadzw. dr hab. inż.',
  'prof zw. dr inż.', 'prof. zw. dr hab.', 'prof. zw. dr hab. inż.', 'licencjat'
}
