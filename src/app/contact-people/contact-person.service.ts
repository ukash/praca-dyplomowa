import 'rxjs/add/operator/toPromise';

import { Injectable } from '@angular/core';
import { ContactPerson } from './contactPerson';
import { Http, Headers } from '@angular/http';

@Injectable()
export class ContactPersonService {

  private contactPeopleUrl = 'api/contactPeople';
  private headers = new Headers({ 'Content-Type': 'application/json' });

  constructor(
    private http: Http
  ) { }

  private handleError(error: any): Promise<any> {
    return Promise.reject(error.json().message || 'Server error');
  }

  getContactPeople(): Promise<ContactPerson[]> {
    return this.http.get(this.contactPeopleUrl)
      .toPromise()
      .then(res => res.json() as ContactPerson[])
      .catch(this.handleError);
  }

  create(contactPerson: ContactPerson): Promise<ContactPerson> {
    return this.http.post(this.contactPeopleUrl, JSON.stringify({
        firstName: contactPerson.firstName,
        lastName: contactPerson.lastName,
        title: contactPerson.title,
        website: contactPerson.website
      }),
      { headers: this.headers })
      .toPromise()
      .then(res => res.json() as ContactPerson)
      .catch(this.handleError);
  }

  update(contactPerson: ContactPerson): Promise<ContactPerson> {
    const url = this.contactPeopleUrl + '/' + contactPerson._id;
    return this.http.put(url, contactPerson, { headers: this.headers })
      .toPromise()
      .then(res => res.json() as ContactPerson)
      .catch(this.handleError);
  }

  delete(id: string): Promise<ContactPerson> {
    const url = this.contactPeopleUrl + '/' + id;
    return this.http.delete(url, { headers: this.headers })
      .toPromise()
      .then(res => res.json() as ContactPerson)
      .catch(this.handleError);
  }

}
