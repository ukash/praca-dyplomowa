import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContactPeopleComponent } from './contact-people.component';

describe('ContactPeopleComponent', () => {
  let component: ContactPeopleComponent;
  let fixture: ComponentFixture<ContactPeopleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContactPeopleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContactPeopleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
