import { Component, OnInit } from '@angular/core';
import { ContactPersonService } from './contact-person.service';
import { MatDialog, MatSnackBar } from '@angular/material';
import { ContactPerson } from './contactPerson';
import { Validators, FormControl } from '@angular/forms';
import { AppContactPeopleRemoveDialogComponent } from './remove-dialog/remove-dialog.component';

const URL_REGEX = /[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/;

@Component({
  selector: 'app-contact-people',
  templateUrl: './contact-people.component.html',
  styleUrls: ['./contact-people.component.css'],
  providers: [ContactPersonService]
})
export class ContactPeopleComponent implements OnInit {

  contactPeople: ContactPerson[];
  selectedContactPerson: ContactPerson;
  titles = ['inż.', 'dypl. ek.', 'mgr', 'mgr inż.', 'mgr inż. architekt', 'dr',
    'dr inż.', 'dr hab.', 'dr hab. inż.', 'dr inż. architekt', 'prof. dr hab.',
    'prof. dr hab. inż.', 'prof. nadzw. dr hab.', 'prof. nadzw. dr hab. inż.',
    'prof zw. dr inż.', 'prof. zw. dr hab.', 'prof. zw. dr hab. inż.', 'licencjat'];
  saveLoading = false;

  urlFormControl = new FormControl('', [
    Validators.pattern(URL_REGEX)]);

  titleFormControl = new FormControl('', [
    Validators.required,
  ]);

  firstNameFormControl = new FormControl('', [
    Validators.required,
  ]);

  lastNameFormControl = new FormControl('', [
    Validators.required,
  ]);

  constructor(
    private contactPersonService: ContactPersonService,
    public dialog: MatDialog,
    public snackbar: MatSnackBar
  ) {
    this.selectedContactPerson = new ContactPerson();
  }

  ngOnInit() {
    this.getContactPeople();
  }

  getContactPeople() {
    this.contactPersonService.getContactPeople().then(contactPeople => this.contactPeople = contactPeople);
  }

  onSelect(contactPerson: ContactPerson): void {
    this.selectedContactPerson = contactPerson;
  }

  openRemoveDialog(contactPerson): void {
    const dialogRef = this.dialog.open(AppContactPeopleRemoveDialogComponent, {
      data: contactPerson
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        contactPerson.removeLoading = true;
        this.delete(contactPerson);
      }
    });
  }

  save(contactPerson: ContactPerson) {
    this.saveLoading = true;
    if (contactPerson._id) {
      this.update(contactPerson);
    } else { this.add(contactPerson); }
  }

  clean() {
    this.selectedContactPerson = new ContactPerson();
    this.urlFormControl.markAsUntouched();
    this.titleFormControl.markAsUntouched();
    this.firstNameFormControl.markAsUntouched();
    this.lastNameFormControl.markAsUntouched();
  }

  add(contactPerson: ContactPerson): void {

    if (!contactPerson) { return; }
    this.contactPersonService.create(contactPerson)
      .then(res => {

        this.contactPeople.push(res);
        this.clean();
        this.saveLoading = false;

        this.snackbar.open('Nowa osoba kontaktowa została dodana.', 'OK', {
          duration: 3000
        });

      }, error => {
        this.saveLoading = false;
        this.snackbar.open('BŁĄD: ' + error, 'OK', {
          duration: 5000
        });
      });
  }

  update(contactPerson: ContactPerson): void {
    this.contactPersonService.update(contactPerson)
      .then(res => {

        const index = this.contactPeople.findIndex(d => d._id === contactPerson._id);
        this.contactPeople[index] = Object.assign({}, res);
        this.clean();
        this.saveLoading = false;

        this.snackbar.open('Dane osoby kontaktowej zostały zmodyfikowane.', 'OK', {
          duration: 3000
        });

      }, error => {
        this.saveLoading = false;
        this.snackbar.open('BŁĄD: ' + error, 'OK', {
          duration: 5000
        });
      });
  }

  delete(contactPerson): void {

    const index = this.contactPeople.indexOf(contactPerson, 0);

    if (index > -1) {
      this.contactPersonService.delete(contactPerson._id)
        .then(res => {

          this.contactPeople.splice(index, 1);

          if (contactPerson === this.selectedContactPerson) {
            this.selectedContactPerson._id = null;
          }

          this.snackbar.open('Osoba kontaktowa została usunięta.', 'OK', {
            duration: 3000
          });

        }, error => {
          contactPerson.removeLoading = undefined;
          this.snackbar.open('BŁĄD: ' + error, 'OK', {
            duration: 5000
          });
        });
    }
  }

}
