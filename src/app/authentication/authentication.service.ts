import 'rxjs/Rx';
import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { User } from '../users/user';

@Injectable()
export class AuthenticationService {
  public user: User;

  public userPromise: Promise<User>;
  private authUrl = 'api/auth';
  private headers = new Headers({ 'Content-Type': 'application/json' });

  // store the URL so we can redirect after logging in
  redirectUrl: string;

  constructor(private http: Http) {

    this.userPromise = this.updateStatus().then(user => this.user = user);
  }

  isLoggedIn(): Observable<boolean> {
    return Observable.fromPromise(this.userPromise).map(() => !!this.user);
  }

  isAdmin(): Observable<boolean> {
    return Observable.fromPromise(this.userPromise).map(() => this.user.role === 'admin' ? true : false);
  }

  signin(user: User): Observable<any> {
    const url = this.authUrl + '/signin';

    return this.http.post(url, user, { headers: this.headers })
      .map(res => this.user = res.json() as User)
      .catch(this.handleObservableError);
  }

  checkStatus(): Observable<User> {
    return Observable.fromPromise(this.userPromise).map(() => this.user);
  }

  updateStatus(): Promise<User> {
    return this.http.get(this.authUrl)
      .toPromise()
      .then(response => response.json() as User)
      .catch(this.handlePromiseError);
  }

  selfUpdate(user: User): Promise<User> {
    const url = this.authUrl + '/update';
    return this.http.put(url, user, { headers: this.headers })
      .toPromise()
      .then(res => this.user = res.json() as User)
      .catch(this.handlePromiseError);
  }

  private handleObservableError(error: Response) {
    return Observable.throw(error.json().message || 'Server error');
  }

  private handlePromiseError(error: any): Promise<any> {
    return Promise.reject(error.json().message || 'Server error');
  }
}
