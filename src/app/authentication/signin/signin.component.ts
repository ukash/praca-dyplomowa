import { Component } from '@angular/core';
import { NavComponent } from '../../nav/nav.component';
import { Location } from '@angular/common';

import { AuthenticationService } from '../authentication.service';
import { User } from '../../users/user';
import { Validators, FormControl } from '@angular/forms';
import { MatSnackBar } from '@angular/material';
import { Router } from '@angular/router';

// tslint:disable-next-line:max-line-length
const EMAIL_REGEX = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html'
})
export class SigninComponent {
  errorMessage: string;
  user: any = {};
  hide = true;
  signinLoading = false;

  emailFormControl = new FormControl('', [
    Validators.required,
    Validators.pattern(EMAIL_REGEX)
  ]);

  passwordFormControl = new FormControl('', [
    Validators.required,
    Validators.minLength(7)
  ]);

  constructor(
    private authenticationService: AuthenticationService,
    private location: Location,
    public snackBar: MatSnackBar,
    public router: Router
  ) { }

  signin() {
    this.signinLoading = true;
    this.authenticationService.signin(this.user).subscribe(result => {
      NavComponent.updateUserStatus.next();
      this.snackBar.open('Zalogowano jako ' + result.firstName + ' ' + result.lastName, 'OK', {
        duration: 3000
      });

      // Get the redirect URL from our auth service
      // If no redirect has been set, use location.back()
      this.authenticationService.redirectUrl ? this.router.navigate([this.authenticationService.redirectUrl]) : this.location.back();

    }, error => {
      this.signinLoading = false;
      this.snackBar.open('BŁĄD: ' + error, 'OK', {
        duration: 5000
      });
    });
  }
}
