import { Routes } from '@angular/router';

import { AuthenticationComponent } from './authentication.component';
import { SigninComponent } from './signin/signin.component';
import { UpdateComponent } from './update/update.component';
import { AuthGuard } from '../auth-guard.service';

export const AuthenticationRoutes: Routes = [{
  path: 'authentication',
  component: AuthenticationComponent,
  children: [
    { path: 'signin', component: SigninComponent },
    { path: 'update', component: UpdateComponent, canActivate: [AuthGuard] }
  ],
}];
