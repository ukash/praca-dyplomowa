import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { AuthenticationRoutes } from './authentication.routes';
import { AuthenticationComponent } from './authentication.component';
import { SigninComponent } from './signin/signin.component';
import { MaterialModule } from '../material.module';
import { UpdateComponent } from './update/update.component';

@NgModule({
  imports: [
    ReactiveFormsModule,
    CommonModule,
    FormsModule,
    RouterModule.forChild(AuthenticationRoutes),
    MaterialModule
  ],
  declarations: [
    AuthenticationComponent,
    SigninComponent,
    UpdateComponent
  ]
})
export class AuthenticationModule { }
