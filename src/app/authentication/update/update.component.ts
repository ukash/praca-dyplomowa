import { Component, OnInit } from '@angular/core';
import { User } from '../../users/user';
import { FormControl, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material';
import { AuthenticationService } from '../authentication.service';
import { UserService } from '../../users/user.service';
import { NavComponent } from '../../nav/nav.component';

const EMAIL_REGEX = /^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;

@Component({
  selector: 'app-update',
  templateUrl: './update.component.html',
  styleUrls: ['./update.component.css']
})
export class UpdateComponent implements OnInit {

  user: User;
  hidePassword = true;
  updateLoading = false;

  emailFormControl = new FormControl('', [
    Validators.required,
    Validators.pattern(EMAIL_REGEX)
  ]);

  firstNameFormControl = new FormControl('', [
    Validators.required,
  ]);
  lastNameFormControl = new FormControl('', [
    Validators.required,
  ]);
  passwordFormControl = new FormControl('', [
    Validators.minLength(7)
  ]);

  constructor(
    public snackbar: MatSnackBar,
    public authenticationService: AuthenticationService
  ) {
    this.user = new User();
  }

  ngOnInit() {
    this.authenticationService.checkStatus().subscribe(user => this.user = user);
  }

  update() {

    this.updateLoading = true;

    this.authenticationService.selfUpdate(this.user)
      .then(user => {

        this.updateLoading = false;
        NavComponent.updateUserStatus.next();
        this.user = user;

        this.snackbar.open('Dane użytkownika zostały zaktualizowane.', 'OK', {
          duration: 3000
        });

      }, error => {
        this.updateLoading = false;
        this.snackbar.open('BŁĄD: ' + error, 'OK', {
          duration: 5000
        });
      });
  }

}
