import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from './material.module';
import { FlexLayoutModule } from '@angular/flex-layout';

import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import { OffersComponent } from './offers/offers.component';
import { OfferDetailsComponent } from './offers/offer-details/offer-details.component';
import { AuthenticationService } from './authentication/authentication.service';
import { AuthenticationModule } from './authentication/authentication.module';
import { NavComponent } from './nav/nav.component';
import { SaveOfferComponent } from './offers/save-offer/save-offer.component';
import { DepartmentsComponent } from './departments/departments.component';
import { ContactPeopleComponent } from './contact-people/contact-people.component';
import { FileSizePipe } from './pipes/file-size.pipe';
import { UsersComponent } from './users/users.component';
import { AuthGuard } from './auth-guard.service';
import { AdminGuard } from './admin-guard.service';
import { FooterComponent } from './footer/footer.component';

@NgModule({
  declarations: [
    AppComponent,
    OffersComponent,
    OfferDetailsComponent,
    UsersComponent,
    NavComponent,
    SaveOfferComponent,
    DepartmentsComponent,
    ContactPeopleComponent,
    FileSizePipe,
    FooterComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    AuthenticationModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MaterialModule,
    ReactiveFormsModule,
    FlexLayoutModule
  ],
  providers: [AuthenticationService, AuthGuard, AdminGuard],
  bootstrap: [AppComponent]
})
export class AppModule {


}
