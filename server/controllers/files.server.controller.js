// Load the module dependencies
const mongoose = require('mongoose');
const File = mongoose.model('File');
const multer = require('multer');
const config = require('../../config/config')
const fs = require('fs');

// Upload a file
exports.upload = function (req, res) {

    const upload = multer({ dest: config.uploadTo }).single('attachment');

    upload(req, res, (err) => {
        if (err) {
            // An error occurred while uploading
            return res.status(422).send({
                message: getErrorMessage(err)
            });
        } else {
            // Create a new file object
            let file = new File(req.file);

            // Try saving the file 
            file.save((err) => {
                if (err) {
                    // Remove a file 
                    fs.unlink(file.path);
                    // If an error occurs send the error message
                    return res.status(400).send({
                        message: getErrorMessage(err)
                    });
                } else {
                    // Remove sensitive data from file
                    file.encoding = undefined;
                    file.destination = undefined;
                    file.filename = undefined;
                    file.path = undefined;

                    // Send a JSON representation of the file info
                    res.json(file)
                }
            });
        }
    });
};

// Returns an existing file info
exports.read = function (req, res) {

    // Get the file from the 'request' object
    const file = req.file;

    // Remove sensitive data from file
    file.encoding = undefined;
    file.destination = undefined;
    file.filename = undefined;
    file.path = undefined;

    // Send a JSON representation of the file info
    res.json(file);
};

// Delete a file
exports.delete = function (req, res) {

    // Get the file from the 'request' object
    const file = req.file;

    // Use the model 'remove' method to delete the file info
    file.remove((err) => {
        if (err) {
            // If an error occurs send the error message
            return res.status(400).send({
                message: getErrorMessage(err)
            });
        } else {
            // Remove a file 
            fs.unlink(file.path);

            // Remove sensitive data from file
            file.encoding = undefined;
            file.destination = undefined;
            file.filename = undefined;
            file.path = undefined;

            // Send a JSON representation of the file info
            res.json(file);
        }
    });
}

// Download a file
exports.download = function (req, res) {
    res.download(req.file.path, req.file.originalname);
}

// Retrieve a single existing file info
exports.fileById = function (req, res, next, id) {
    // Use the model 'findById' method to find a single file info
    File.findById(id).exec((err, file) => {
        if (err) return next(err);
        if (!file) return next(new Error('Nie udało się załadować pliku #' + id));

        // If an file info is found use the 'request' object to pass it to the next middleware
        req.file = file;

        // Call the next middleware
        next();
    });
};

// Error handling controller method
const getErrorMessage = function (err) {
    if (err.errors) {
        for (const errName in err.errors) {
            if (err.errors[errName].message) return err.errors[errName].message;
        }
    } else {
        return 'Coś poszło nie tak...';
    }
};

