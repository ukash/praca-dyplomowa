// Load the module dependencies
const mongoose = require('mongoose');
const Offer = mongoose.model('Offer');

// Retrieve a list of offers
exports.list = function (req, res) {

    let sendHidden = null;
    let params = {
        "extId": 1,
        "topic": 1,
        "department": 1,
        "trl": 1,
        "contactPeople": 1,
        "tags": 1
    };

    // If a user is not authenticated don't send hidden offers
    if (!req.isAuthenticated()) { sendHidden = { "visible": true } }
    else { params["visible"] = 1; }

    // Use the model 'find' method to get a list of offers
    Offer
        .find(sendHidden, params)
        .sort('extId')
        .populate('department', 'name')
        .populate('contactPeople', 'firstName lastName')
        .exec((err, offers) => {
            if (err) {
                // If an error occurs send the error message
                return res.status(400).send({
                    message: getErrorMessage(err)
                });
            } else {
                // Send a JSON representation of the offer
                res.json(offers);
            }
        });
};

// Retrieve a list of offers
exports.listLess = function (req, res) {
    // Use the model 'find' method to get a list of offers
    Offer.find({}, { "extId": 1, "topic": 1 })
        .sort('extId')
        .exec((err, offers) => {
            if (err) {
                // If an error occurs send the error message
                return res.status(400).send({
                    message: getErrorMessage(err)
                });
            } else {
                // Send a JSON representation of the offer
                res.json(offers);
            }
        });
};

// Retrieve a list of tags from offers
exports.listTags = function (req, res) {

    // Get an array of all tags w/ count
    Offer.aggregate([
        { "$project": { "tags": 1 } },
        { "$unwind": "$tags" },
        { "$group": { "_id": "$tags", "count": { "$sum": 1 } } },
        { "$sort": { "count": -1 } }
    ]).exec((err, tags) => {
        if (err) {
            // If an error occurs send the error message
            return res.status(400).send({
                message: getErrorMessage(err)
            });
        } else {
            // Send a JSON representation of the array of tags
            res.json(tags);
        }
    });
};

// Create a new offer
exports.create = function (req, res) {

    // Create a new offer object
    let offer = new Offer(req.body);

    // Sort relatedOffers
    offer.relatedOffers.sort((a, b) => a > b);

    // Set the offer's 'creator' property
    offer.createdBy = offer.updatedBy = req.user;

    // Find next extId
    Offer.find({}, { extId: 1 }).sort({ extId: -1 }).limit(1).exec((err, lastId) => {
        if (err) {
            // If an error occurs send the error message
            return res.status(400).send({
                message: getErrorMessage(err)
            });
        } else {
            offer.extId = (!lastId.length ? 1 : ++lastId[0].extId);

            // Save the offer
            saveOffer(offer, (err, savedOffer) => {
                if (err) {
                    // If an error occurs send the error message
                    return res.status(400).send({
                        message: getErrorMessage(err)
                    });
                } else {
                    // Send a JSON representation of the offer
                    res.json(savedOffer);
                }
            });
        }
    });
};

// Returns an existing offer
exports.read = function (req, res) {
    res.json(req.offer);
};

// Updates an existing offer
exports.update = function (req, res) {

    // Get the offer from the 'request' object
    const offer = req.offer;

    // Update the offer fields
    offer.topic = req.body.topic;
    offer.abstract = req.body.abstract;
    offer.benefits = req.body.benefits;
    offer.specification = req.body.specification;
    offer.techDetails = req.body.techDetails;
    offer.applications = req.body.applications;
    offer.trl = req.body.trl;
    offer.patents = req.body.patents;
    offer.ipStatuses = req.body.ipStatuses;
    offer.partnerships = req.body.partnerships;
    offer.department = req.body.department;
    offer.contactPeople = req.body.contactPeople;
    offer.tags = req.body.tags;
    offer.relatedOffers = req.body.relatedOffers;
    offer.visible = req.body.visible;
    offer.attachments = req.body.attachments;

    // Set the update's 'creator' property
    offer.updatedBy = req.user;

    // Set the offer creator if not found
    if (!offer.createdBy) { offer.createdBy = req.user }

    // Try saving the updated offer
    offer.save((err) => {
        if (err) {
            // If an error occurs send the error message
            return res.status(400).send({
                message: getErrorMessage(err)
            });
        } else {
            // Send a JSON representation of the offer
            res.json(offer);
        }
    });
};

// Delete an existing offer
exports.delete = function (req, res) {
    // Get the offer from the 'request' object
    const offer = req.offer;

    // Use the model 'remove' method to delete the offer
    offer.remove((err) => {
        if (err) {
            // If an error occurs send the error message
            return res.status(400).send({
                message: getErrorMessage(err)
            });
        } else {
            // Send a JSON representation of the offer
            res.json(offer);
        }
    });
};

// Retrieve a single existing offer
exports.offerById = function (req, res, next, id) {
    // Use the model 'findById' method to find a single offer
    Offer.findById(id)
        .populate('createdBy', 'firstName lastName')
        .populate('updatedBy', 'firstName lastName')
        .populate('department')
        .populate('contactPeople')
        .populate('relatedOffers', 'extId topic')
        .populate('attachments', 'originalname mimetype size')
        .exec((err, offer) => {
            if (err) return next(err);
            if (!offer) return next(new Error('Nie udało się załadować oferty #' + id));

            // If an offer is found use the 'request' object to pass it to the next middleware
            req.offer = offer;

            // Call the next middleware
            next();
        });
};

// Error handling controller method
const getErrorMessage = function (err) {
    if (err.errors) {
        for (const errName in err.errors) {
            if (err.errors[errName].message) return err.errors[errName].message;
        }
    } else {
        return 'Coś poszło nie tak...';
    }
};

// Saving offer function which increment extId on failure
const saveOffer = function (offer, savedOffer) {

    offer.save((err) => {
        if (err) {
            if (err.code === 11000) {    /* dup key */
                offer.extId++;
                saveOffer(offer, savedOffer);
            }
            else {
                // If an error occurs send the error
                savedOffer(err);
            }

        } else {
            // Send the saved offer
            savedOffer(null, offer);
        }
    });
}