// Load the module dependencies
const mongoose = require('mongoose');
const ContactPerson = mongoose.model('ContactPerson');

// Retrieve a list of contactPersons
exports.list = function (req, res) {
    // Use the model 'find' method to get a list of contactPersons
    ContactPerson.find().exec((err, contactPersons) => {
        if (err) {
            // If an error occurs send the error message
            return res.status(400).send({
                message: getErrorMessage(err)
            });
        } else {
            // Send a JSON representation of the contactPerson
            res.json(contactPersons);
        }
    });
};

// Create a new contactPerson
exports.create = function (req, res) {

    // Create a new contactPerson object
    const contactPerson = new ContactPerson(req.body);

    // Try saving the contactPerson
    contactPerson.save((err) => {
        if (err) {
            // If an error occurs send the error message
            return res.status(400).send({
                message: getErrorMessage(err)
            });
        } else {
            // Send a JSON representation of the contactPerson
            res.json(contactPerson);
        }
    });
};

// Returns an existing contactPerson
exports.read = function (req, res) {
    res.json(req.contactPerson);
};

// Updates an existing contactPerson
exports.update = function (req, res) {
    // Get the contactPerson from the 'request' object
    const contactPerson = req.contactPerson;

    // Update the contactPerson fields
    contactPerson.firstName = req.body.firstName;
    contactPerson.lastName = req.body.lastName;
    contactPerson.title = req.body.title;
    contactPerson.website = req.body.website;

    // Try saving the updated contactPerson
    contactPerson.save((err) => {
        if (err) {
            // If an error occurs send the error message
            return res.status(400).send({
                message: getErrorMessage(err)
            });
        } else {
            // Send a JSON representation of the contactPerson
            res.json(contactPerson);
        }
    });
};

// Delete an existing contactPerson
exports.delete = function (req, res) {
    // Get the contactPerson from the 'request' object
    const contactPerson = req.contactPerson;

    // Use the model 'remove' method to delete the contactPerson
    contactPerson.remove((err) => {
        if (err) {
            // If an error occurs send the error message
            return res.status(400).send({
                message: getErrorMessage(err)
            });
        } else {
            // Send a JSON representation of the contactPerson
            res.json(contactPerson);
        }
    });
};

// Retrieve a single existing contactPerson
exports.contactPersonById = function (req, res, next, id) {
    // Use the model 'findById' method to find a single contactPerson
    ContactPerson.findById(id).exec((err, contactPerson) => {
        if (err) return next(err);
        if (!contactPerson) return next(new Error('Nie udało się załadować osoby kontaktowej #' + id));

        // If an contactPerson is found use the 'request' object to pass it to the next middleware
        req.contactPerson = contactPerson;

        // Call the next middleware
        next();
    });
};

// Error handling controller method
const getErrorMessage = function (err) {
    if (err.errors) {
        for (const errName in err.errors) {
            if (err.errors[errName].message) return err.errors[errName].message;
        }
    } else {
        return 'Coś poszło nie tak...';
    }
};

