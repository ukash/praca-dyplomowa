// Load the module dependencies
const mongoose = require('mongoose');
const Department = mongoose.model('Department');
const Offer = mongoose.model('Offer');

// Retrieve a list of departments
exports.list = function (req, res) {
    // Use the model 'find' method to get a list of departments
    Department.find().exec((err, departments) => {
        if (err) {
            // If an error occurs send the error message
            return res.status(400).send({
                message: getErrorMessage(err)
            });
        } else {
            // Send a JSON representation of the department
            res.json(departments);
        }
    });
};

// Create a new department
exports.create = function (req, res) {

    // Create a new department object
    const department = new Department(req.body);

    // Try saving the department
    department.save((err) => {
        if (err) {
            // If an error occurs send the error message
            return res.status(400).send({
                message: getErrorMessage(err)
            });
        } else {
            // Send a JSON representation of the department
            res.json(department);
        }
    });
};

// Returns an existing department
exports.read = function (req, res) {
    res.json(req.department);
};

// Updates an existing department
exports.update = function (req, res) {
    // Get the department from the 'request' object
    const department = req.department;

    // Update the department fields
    department.name = req.body.name;
    department.website = req.body.website;

    // Try saving the updated department
    department.save((err) => {
        if (err) {
            // If an error occurs send the error message
            return res.status(400).send({
                message: getErrorMessage(err)
            });
        } else {
            // Send a JSON representation of the department
            res.json(department);
        }
    });
};

// Delete an existing department
exports.delete = function (req, res) {
    // Get the department from the 'request' object
    const department = req.department;

    Offer.count({ department: department._id }).exec((err, count) => {
        if (err) {
            // If an error occurs send the error message
            return res.status(400).send({
                message: getErrorMessage(err)
            });
        }
        if (count) {
            return res.status(400).send({
                message: 'Nie można usunąć katedry, do której przypisane są oferty.'
            });
        }

        // Use the model 'remove' method to delete the department
        department.remove((err) => {
            if (err) {
                // If an error occurs send the error message
                return res.status(400).send({
                    message: getErrorMessage(err)
                });
            } else {
                // Send a JSON representation of the department
                res.json(department);
            }
        });
    })
};

// Retrieve a single existing department
exports.departmentById = function (req, res, next, id) {
    // Use the model 'findById' method to find a single department
    Department.findById(id).exec((err, department) => {
        if (err) return next(err);
        if (!department) return next(new Error('Nie udało się załadować katedry #' + id));

        // If an department is found use the 'request' object to pass it to the next middleware
        req.department = department;

        // Call the next middleware
        next();
    });
};

// Error handling controller method
const getErrorMessage = function (err) {
    if (err.errors) {
        for (const errName in err.errors) {
            if (err.errors[errName].message) return err.errors[errName].message;
        }
    } else {
        return 'Coś poszło nie tak...';
    }
};

