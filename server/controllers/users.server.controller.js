// Load the module dependencies
const User = require('mongoose').model('User');
const crypto = require('crypto');
const passport = require('passport');

// Error handling controller method
const getErrorMessage = function (err) {
	if (err.errors) {
		for (const errName in err.errors) {
			if (err.errors[errName].message) return err.errors[errName].message;
		}
	} else {
		return 'Coś poszło nie tak...';
	}
};

// Sign error handling controller method
const getSignErrorMessage = function (err) {
	// Define the error message variable
	let message = '';

	// If an internal MongoDB error occurs get the error message
	if (err.code) {
		switch (err.code) {
			// If a unique index error occurs set the message error
			case 11000:
			case 11001:
				message = 'Adres email jest zajęty.';
				break;
			// If a general error occurs set the message error
			default:
				message = 'Coś poszło nie tak...';
		}
	} else {
		// Grab the first error message from a list of possible errors
		for (const errName in err.errors) {
			if (err.errors[errName].message) message = err.errors[errName].message;
		}
	}

	// Return the message error
	return message;
};

// Create a new controller method that signin users
exports.signin = function (req, res, next) {
	passport.authenticate('local', (err, user, info) => {
		if (err || !user) {
			res.status(400).send(info);
		} else {
			// Remove sensitive data before login
			user.password = undefined;
			user.salt = undefined;

			// Use the Passport 'login' method to login
			req.login(user, (err) => {
				if (err) {
					res.status(400).send(err);
				} else {
					res.json(user);
				}
			});
		}
	})(req, res, next);
};

// Create a new controller method that creates new users
exports.signUser = function (req, res) {
	const user = new User(req.body);

	// Try saving the User
	user.save((err) => {
		if (err) {
			return res.status(400).send({
				message: getSignErrorMessage(err)
			});
		} else {
			// Remove sensitive data
			user.password = undefined;
			user.salt = undefined;

			// Login user data
			res.json(user);
		}
	});
}

// Create a new controller method that creates new 'regular' users
exports.signup = function (req, res) {
	const user = new User(req.body);

	// Try saving the User
	user.save((err) => {
		if (err) {
			return res.status(400).send({
				message: getSignErrorMessage(err)
			});
		} else {
			// Remove sensitive data before login
			user.password = undefined;
			user.salt = undefined;

			// Login the user
			req.login(user, function (err) {
				if (err) {
					res.status(400).send(err);
				} else {
					res.json(user);
				}
			});
		}
	});
}

// Create a new controller method for signing out
exports.signout = function (req, res) {
	// Use the Passport 'logout' method to logout
	req.logout();

	// Redirect the user back to the main application page
	res.redirect('/');
};

// Returns an existing user
exports.read = function (req, res) {

	const user = new User(req.selectedUser);

	user.salt = undefined;
	user.password = undefined;

	res.json(user);
};

// Delete an existing user
exports.delete = function (req, res) {
	// Get the user from the 'request' object
	const user = req.selectedUser;

	// Use the model 'remove' method to delete the user
	user.remove((err) => {
		if (err) {
			// If an error occurs send the error message
			return res.status(400).send({
				message: getErrorMessage(err)
			});
		} else {
			// Remove sensitive data
			user.password = undefined;
			user.salt = undefined;

			// Send a JSON representation of the user
			res.json(user);
		}
	});
};

// Updates a user whose made a request
exports.selfUpdate = function (req, res) {

	// Try saving the updated user
	User.findOneAndUpdate({ _id: req.user._id }, req.body, { new: true })
		.exec((err, user) => {
			if (err) {
				// If an error occurs send the error message
				return res.status(400).send({
					message: getErrorMessage(err)
				});
			} else {
				// Remove sensitive data
				user.password = undefined;
				user.salt = undefined;

				// Send a JSON representation of the user
				res.json(user);
			}
		});
};

// Returns an existing department
exports.userInfo = function (req, res) {
	const user = (!req.user) ? null : {
		_id: req.user.id,
		email: req.user.email,
		firstName: req.user.firstName,
		lastName: req.user.lastName,
		fullName: `${req.user.firstName} ${req.user.lastName}`,
		role: req.user.role
	};

	res.json(user);
};

// Updates an existing user
exports.update = function (req, res) {

	// Try saving the updated user
	User.findOneAndUpdate({ _id: req.selectedUser._id }, req.body, { new: true })
		.exec((err, user) => {
			if (err) {
				// If an error occurs send the error message
				return res.status(400).send({
					message: getErrorMessage(err)
				});
			} else {
				// Remove sensitive data
				user.password = undefined;
				user.salt = undefined;

				// Send a JSON representation of the user
				res.json(user);
			}
		});
};

// Retrieve a list of users
exports.list = function (req, res) {
	// Use the model 'find' method to get a list of users
	User.find({}, {
		"firstName": 1,
		"lastName": 1,
		"email": 1,
		"role": 1
	}).exec((err, users) => {
		if (err) {
			// If an error occurs send the error message
			return res.status(400).send({
				message: getErrorMessage(err)
			});
		} else {
			// Send a JSON representation of the users
			res.json(users);
		}
	});
};

// Retrieve a single existing user
exports.userById = function (req, res, next, id) {
	// Use the model 'findById' method to find a single user
	User.findById(id).exec((err, user) => {
		if (err) return next(err);
		if (!user) return next(new Error('Nie udało się załadować użytkownika #' + id));

		// If an user is found use the 'request' object to pass it to the next middleware
		req.selectedUser = user;

		// Call the next middleware
		next();
	});
};

// Create a new controller middleware that is used to authorize authenticated operations
exports.requiresLogin = function (req, res, next) {
	// If a user is not authenticated send the appropriate error message
	if (!req.isAuthenticated()) {
		return res.status(401).send({
			message: 'Użytkownik nie jest zalogowany.'
		});
	}

	// Call the next middleware
	next();
};

// Create a new controller middleware that is used to authorize authenticated operations
exports.isAdmin = function (req, res, next) {
	// If a user is not authenticated send the appropriate error message
	if (!req.isAuthenticated() || req.user.role !== 'admin') {
		return res.status(401).send({
			message: 'Admin nie jest zalogowany.'
		});
	}

	// Call the next middleware
	next();
};
