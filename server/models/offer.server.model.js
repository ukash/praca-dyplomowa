// Load the Mongoose module and Schema object
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// Define a new 'OfferSchema'
const OfferSchema = new Schema({

    extId: {
        type: Number,
        unique: true,
        required: true
    },
    topic: {
        type: String,
        // Validate 'topic' value existance
        required: 'Tytuł oferty jest wymagany.'
    },
    abstract: {
        type: String,
        required: 'Krótki opis oferty jest wymagany.'
    },
    benefits: [String],
    specification: {
        type: String,
        // Trim the 'specification' field
        trim: true
    },
    techDetails: [{
        name: String,
        value: String
    }],
    applications: [String],
    trl: {
        type: String,
        enum: [
            '1 - Zaobserwowano podstawowe zasady danego zjawiska',
            '2 - Sformułowano koncepcję technologiczną',
            '3 - Przeprowadzono eksperymentalny dowód na słuszność koncepcji',
            '4 - Przeprowadzono walidację technologii w warunkach laboratoryjnych',
            '5 - Dokonano walidacji technologii w środowisku zbliżonym do rzeczywistego',
            '6 - Dokonano demonstracji technologii w środowisku zbliżonym do rzeczywistego',
            '7 - Dokonano demonstracji prototypu systemu w otoczeniu operacyjnym',
            '8 - Zakończono badania i demonstrację ostatecznej formy technologii',
            '9 - Działanie systemu udowodniono w środowisku operacyjnym i uruchomiono produkcję na skalę przemysłową'
        ],
        default: '1 - Zaobserwowano podstawowe zasady danego zjawiska'
    },
    patents: [{
        title: String,
        url: {
            type: String,
            trim: true,
            match: [/[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/,
                'Wprowadzony adres strony www patentu jest nieprawidłowy.'],
        }
    }],
    ipStatuses: [String],
    partnerships: [String],
    attachments: [{
        type: Schema.ObjectId,
        ref: 'File'
    }],
    department: {
        type: Schema.ObjectId,
        ref: 'Department',
        required: 'Wybór katedry jest wymagany.'
    },
    contactPeople: [{
        type: Schema.ObjectId,
        ref: 'ContactPerson',
    }],
    tags: [{
        type: String,
        // Trim and lowercase the 'tag' to unify input data
        lowercase: true,
        trim: true
    }],
    relatedOffers: [{
        type: Schema.ObjectId,
        ref: 'Offer'
    }],
    visible: {
        type: Boolean,
        required: true,
        default: true
    },
    createdBy: {
        type: Schema.ObjectId,
        ref: 'User',
        required: true
    },
    updatedBy: {
        type: Schema.ObjectId,
        ref: 'User',
        required: true
    }
},
    {
        timestamps: true
    }
);

OfferSchema.pre('save', function (next) {

    // Prepending "http://" to a URL that doesn't already contain "http://" or "https://"
    for (let patent of this.patents) {
        if (patent.url && !/^https?:\/\//i.test(patent.url)) {
            patent.url = 'http://' + patent.url;
        }
    }
    next();
});

// Create the 'Offer' model out of the 'OfferSchema'
mongoose.model('Offer', OfferSchema);
