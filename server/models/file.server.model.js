// Load the Mongoose module and Schema object
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// Define a new 'FileSchema'
const FileSchema = new Schema({

    // Name of the file on the user's computer
    originalname: String,
    // Encoding type of the file
    encoding: String,
    // Mime type of the file
    mimetype: String,
    // Size of the file in bytes
    size: Number,
    // The folder to which the file has been saved
    destination: String,
    // The name of the file within the destination
    filename: String,
    // The full path to the uploaded file
    path: String, 
});

// Create the 'File' model out of the 'FileSchema'
mongoose.model('File', FileSchema);
