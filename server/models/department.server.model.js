// Load the Mongoose module and Schema object
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// Define a new 'DepartmentSchema'
const DepartmentSchema = new Schema({

  name: {
    type: String,
    // Trim the 'specification' field
    trim: true,
    // Validate 'name' value existance
    required: 'Nazwa katedry jest wymagana.'
  },
  website: {
    type: String,
    trim: true,
    // Validate the url
    match: [/[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/,
      'Wprowadzony adres strony www jest nieprawidłowy.'],
    // Validate 'name' value existance
    required: 'Adres strony www katedry jest wymagany.'
  }

});

DepartmentSchema.virtual('initialism')
  .get(function () {

    const splitName = this.name.split(' ');

    let initialism = '';

    for (init of splitName)
      initialism += init[0];

    return initialism;
  })
  .set(function (fullName) {
    const splitName = fullName.split(' ');
    this.title = splitName[0] || '';
    this.firstName = splitName[1] || '';
    this.lastName = splitName[2] || '';
  });

DepartmentSchema.pre('save', function (next) {

  // Prepending "http://" to a URL that doesn't already contain "http://" or "https://"
  if (this.website && !/^https?:\/\//i.test(this.website)) {
    this.website = 'http://' + this.website;
  }
  next();
});

DepartmentSchema.set('toJSON', {
  getters: true,
  setters: true,
  virtuals: true,
  transform: function (doc, ret) {
    ret.id = undefined;
  }
});

// Create the 'Department' model out of the 'DepartmentSchema'
mongoose.model('Department', DepartmentSchema);
