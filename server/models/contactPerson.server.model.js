// Load the Mongoose module and Schema object
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// Define a new 'ContactPersonSchema'
const ContactPersonSchema = new Schema({

  firstName: {
    type: String,
    // Trim the 'specification' field
    trim: true,
    // Validate 'name' value existance
    required: 'Imię jest wymagane.'
  },
  lastName: {
    type: String,
    trim: true,
    required: 'Nazwisko jest wymagane.'
  },
  title: {
    type: String,
    // Validate the 'title' value using enum list
    enum: {
      values: ['inż.', 'dypl. ek.', 'mgr', 'mgr inż.', 'mgr inż. architekt', 'dr',
        'dr inż.', 'dr hab.', 'dr hab. inż.', 'dr inż. architekt', 'prof. dr hab.',
        'prof. dr hab. inż.', 'prof. nadzw. dr hab.', 'prof. nadzw. dr hab. inż.',
        'prof zw. dr inż.', 'prof. zw. dr hab.', 'prof. zw. dr hab. inż.', 'licencjat'],
      message: 'Wprowadzony tytuł jest nieprawidłowy.'
    }
  },
  website: {
    type: String,
    trim: true,
    // Validate the url
    match: [/[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/,
      'Wprowadzony adres strony www jest nieprawidłowy.']
  }

});

ContactPersonSchema.virtual('fullName')
  .get(function () {
    return (this.title ? (this.title + ' ') : '') + (this.firstName ? this.firstName + ' ' : '') + (this.lastName ? this.lastName : '');
  })
  .set(function (fullName) {
    const splitName = fullName.split(' ');
    this.title = splitName[0] || '';
    this.firstName = splitName[1] || '';
    this.lastName = splitName[2] || '';
  });

ContactPersonSchema.pre('save', function (next) {

  // Prepending "http://" to a URL that doesn't already contain "http://" or "https://"
  if (this.website && !/^https?:\/\//i.test(this.website)) {
    this.website = 'http://' + this.website;
  }
  next();
});

ContactPersonSchema.set('toJSON', {
  getters: true,
  setters: true,
  virtuals: true,
  transform: function (doc, ret) {
    ret.id = undefined;
  }
});

// Create the 'ContactPerson' model out of the 'ContactPersonSchema'
mongoose.model('ContactPerson', ContactPersonSchema);
