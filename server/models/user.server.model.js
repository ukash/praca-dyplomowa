// Load the module dependencies
const mongoose = require('mongoose');
const crypto = require('crypto');
const Schema = mongoose.Schema;

// Define a new 'UserSchema'
const UserSchema = new Schema({

	firstName: {
		type: String,
		required: 'Imię jest wymagane.'
	},
	lastName: {
		type: String,
		required: 'Nazwisko jest wymagane.'
	},
	email: {
		type: String,
		// Set a unique 'e-mail' index
		unique: true,
		// Validate the email format
		match: [/.+\@.+\..+/,
			'Wprowadzono nieprawidłowy adres email.'],
		// Validate 'e-mail' value existance
		required: 'Adres email jest wymagany.',
		// Trim the 'e-mail' field
		trim: true
	},
	role: {
		type: String,
		enum: ['admin', 'user'],
		default: 'user'
	},
	password: {
		type: String,
		required: 'Hasło jest wymagane.',
		// Validate the 'password' value length
		validate: [
			(password) => password && password.length > 6,
			'Hasło jest za krótkie.'
		]
	},
	salt: {
		type: String
	}
},
	{
		timestamps: true
	});

// Use a pre-update middleware to hash the password
UserSchema.pre('findOneAndUpdate', function (next) {
	if (this._update.password) {
		this._update.salt = new Buffer(crypto.randomBytes(16).toString('base64'), 'base64').toString();
		this._update.password = UserSchema.methods.hashPassword(this._update.password, this._update.salt);
	}

	next();
});

// Use a pre-save middleware to hash the password
UserSchema.pre('save', function (next) {
	if (this.password) {
		this.salt = new Buffer(crypto.randomBytes(16).toString('base64'), 'base64');
		this.password = this.hashPassword(this.password);
	}

	next();
});

// Create an instance method for hashing a password
UserSchema.methods.hashPassword = function (password, salt = this.salt) {
	return crypto.pbkdf2Sync(password, salt, 10000, 64, 'sha1').toString('base64');
};

// Create an instance method for authenticating user
UserSchema.methods.authenticate = function (password) {
	return this.password === this.hashPassword(password);
};

UserSchema.virtual('fullName')
	.get(function () {
		return (this.title ? (this.title + ' ') : '') + (this.firstName ? this.firstName + ' ' : '') + (this.lastName ? this.lastName : '');
	})
	.set(function (fullName) {
		const splitName = fullName.split(' ');
		this.title = splitName[0] || '';
		this.firstName = splitName[1] || '';
		this.lastName = splitName[2] || '';
	});

UserSchema.set('toJSON', {
	getters: true,
	setters: true,
	virtuals: true,
	transform: function (doc, ret) {
		ret.id = undefined;
	}
});

// Create the 'User' model out of the 'UserSchema'
mongoose.model('User', UserSchema);
