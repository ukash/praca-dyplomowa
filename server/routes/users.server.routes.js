// Load the module dependencies
const users = require('../controllers/users.server.controller');
const passport = require('passport');

// Define the routes module' method
module.exports = function (app) {

    app.route('/api/users')
        .get(users.isAdmin, users.list)
        .post(users.isAdmin, users.signUser);

    app.route('/api/users/:userId')
        .get(users.isAdmin, users.read)
        .put(users.isAdmin, users.update)
        .delete(users.isAdmin, users.delete);

    app.route('/api/auth/update')
        .put(users.requiresLogin, users.selfUpdate);

    // Set up the 'signin' routes 
    app.route('/api/auth/signin').post(users.signin);

    // Set up the 'signout' route
    app.route('/api/auth/signout').get(users.signout);

    // Set up the 'userInfo' route
    app.route('/api/auth').get(users.userInfo);

    app.param('userId', users.userById);
};