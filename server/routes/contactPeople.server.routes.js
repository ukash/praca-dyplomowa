const users = require('../controllers/users.server.controller');
const contactPeople = require('../controllers/contactPeople.server.controller');

module.exports = function (app) {

    app.route('/api/contactPeople')
        .get(contactPeople.list)
        .post(users.requiresLogin, contactPeople.create);

    app.route('/api/contactPeople/:contactPersonId')
        .get(contactPeople.read)
        .put(users.requiresLogin, contactPeople.update)
        .delete(users.requiresLogin, contactPeople.delete);

    app.param('contactPersonId', contactPeople.contactPersonById);

};