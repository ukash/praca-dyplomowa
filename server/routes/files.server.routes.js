const files = require('../controllers/files.server.controller');
const users = require('../controllers/users.server.controller');

module.exports = function (app) {

    app.route('/api/files')
        .post(users.requiresLogin, files.upload);

    app.route('/api/files/:fileId')
        .get(files.read)
        .delete(users.requiresLogin, files.delete);

    app.route('/api/files/download/:fileId')
        .get(files.download)

    app.param('fileId', files.fileById);

};