const users = require('../controllers/users.server.controller');
const offers = require('../controllers/offers.server.controller');

module.exports = function (app) {

    app.route('/api/offers/tags')
        .get(offers.listTags);

    app.route('/api/offers/less')
        .get(offers.listLess)

    app.route('/api/offers')
        .get(offers.list)
        .post(users.requiresLogin, offers.create);

    app.route('/api/offers/:offerId')
        .get(offers.read)
        .put(users.requiresLogin, offers.update)
        .delete(users.requiresLogin, offers.delete);

    app.param('offerId', offers.offerById);

};