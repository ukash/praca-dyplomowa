const users = require('../controllers/users.server.controller');
const departments = require('../controllers/departments.server.controller');

module.exports = function (app) {

    app.route('/api/departments')
        .get(departments.list)
        .post(users.isAdmin, departments.create);

    app.route('/api/departments/:departmentId')
        .get(departments.read)
        .put(users.isAdmin, departments.update)
        .delete(users.isAdmin, departments.delete);

    app.param('departmentId', departments.departmentById);
};