// Set the 'NODE_ENV' and 'PORT' variable
process.env.NODE_ENV = process.env.NODE_ENV || 'development';
process.env.PORT = process.env.PORT || '3000';

// Load the module dependencies
const configureMongoose = require('./config/mongoose');
const configureExpress = require('./config/express');
const configurePassport = require('./config/passport');

// Create a new Mongoose connection instance
const db = configureMongoose();

// Create a new Express application instance
const app = configureExpress(db);

// Configure the Passport middleware
const passport = configurePassport();

// Use the Express application instance to listen to the process.env.PORT
// and log the server status to the console
app.listen(process.env.PORT, () => console.log('Server running at localhost:' + process.env.PORT));

// Use the module.exports property to expose our Express application instance for external usage
module.exports = app;